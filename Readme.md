﻿#Set up your local directory
##Set up Git on your machine if you haven't already.
* mkdir /path/to/your/project
* cd /path/to/your/project
* git init
* git remote add origin https://gummi1801@bitbucket.org/gummi1801/vsh2a-rwd.git

##Create your first file, commit, and push
* echo "Guðmundur Jón Guðjónsson" >> contributors.txt
* git add contributors.txt
* git commit -m 'Initial commit with contributors'
* git push -u origin master